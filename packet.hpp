#pragma once

#include <array>
#include <cstddef>
#include <cstdint>

constexpr std::size_t MAX_PACKET_SIZE = 65507;

struct Packet {
    std::uint64_t id;
    std::array<std::uint8_t, MAX_PACKET_SIZE - 8> payload;
};

static_assert(sizeof(Packet) >= MAX_PACKET_SIZE);
