#include "packet.hpp"
#include <asio.hpp>
#include <asio/buffer.hpp>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <fmt/format.h>
#include <fmt/os.h>
#include <fstream>
#include <iostream>

using udp = asio::ip::udp;
using clk = std::chrono::steady_clock;

int main(int argc, char *argv[]) {
  asio::io_context context;

  udp::resolver resolver(context);
  udp::endpoint endpoint =
      *resolver.resolve(udp::v4(), argv[1], argv[2]).begin();
  udp::socket s(context, udp::endpoint(udp::v4(), 0));

  int packet_size = argc >= 4 ? std::stoi(argv[3]) : MAX_PACKET_SIZE;
  double time_between_packets = argc >= 5 ? std::stod(argv[4]) : 100;

  std::cout << "packet size: " << packet_size << " bytes" << std::endl;
  std::cout << "send interval: " << time_between_packets << " ms" << std::endl;

  if (packet_size < 8) {
    std::cerr << "packet size must be at least 8 bytes and at most "
              << MAX_PACKET_SIZE << " bytes" << std::endl;
    return -1;
  }

  std::uint64_t bytes_sent = 0;
  auto send_duration_file = fmt::output_file(fmt::format("send_durations_{}_{}.csv", packet_size, time_between_packets));

  std::cout << "theoretical bandwidth: "
            << packet_size * 1000 * 8 / time_between_packets / 1000.0 / 1000.0
            << " Mbit/s" << std::endl;

  const auto dt = std::chrono::duration_cast<clk::duration>(
      std::chrono::duration<double, std::milli>(time_between_packets));

  std::uint64_t skipped = 0;
  Packet packet{.id = 0};
  auto last = clk::now();
  while (true) {
    const auto now = clk::now();

    if (now - last > dt) {
      s.send_to(asio::buffer(&packet, packet_size), endpoint);
      packet.id += 1;
      bytes_sent += packet_size;
      const auto after_send = clk::now();

      send_duration_file.print("{},{}\n", std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(after_send - now).count(), bytes_sent);
      send_duration_file.flush();

      while (last + dt < clk::now()) {
        skipped++;
        std::putchar('.');
        last += dt;
      }
    }
  }
}
