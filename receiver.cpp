#include "packet.hpp"
#include <asio.hpp>
#include <asio/buffer.hpp>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <optional>

using udp = asio::ip::udp;
using clk = std::chrono::steady_clock;

struct ReceivedContext {
  std::uint64_t highest_id_received;
  clk::time_point first_receive;
};

class Receiver {
public:
  Receiver(std::uint16_t port)
      : socket(context, udp::endpoint(udp::v4(), port)) {
    receive();
  }

  void run() { context.run(); }

private:
  asio::io_context context;
  udp::socket socket;
  Packet data;
  std::optional<ReceivedContext> receive_data;
  std::uint64_t total_bytes_received = 0;
  std::uint64_t packets_received = 0;
  std::uint64_t out_of_order_delivered_packets = 0;
  clk::time_point last_print;

  void receive() {
    socket.async_receive(
        asio::buffer(&data, MAX_PACKET_SIZE),
        [this](std::error_code ec, std::size_t bytes_received) {
          if (ec) {
            std::cerr << ec.category().name() << ": " << ec.message()
                      << std::endl;
          } else {
            if (!receive_data.has_value()) {
              // ignore first packet
              receive_data.emplace(ReceivedContext{
                  .highest_id_received = data.id,
                  .first_receive = clk::now(),
              });
            } else {
              packets_received += 1;
              total_bytes_received += bytes_received;

              const auto dt = clk::now() - receive_data->first_receive;
              const auto seconds =
                  std::chrono::duration_cast<std::chrono::duration<double>>(dt)
                      .count();
              // std::cout << "ID: " << data.id << " length: " << bytes_received
              //           << std::endl;
              //
              if (data.id > receive_data->highest_id_received) {
                if (data.id != receive_data->highest_id_received + 1) {
                  out_of_order_delivered_packets += 1;
                }
                receive_data->highest_id_received = data.id;
              }

              if (clk::now() - last_print > std::chrono::seconds(2)) {
                std::cout << total_bytes_received * 8 / seconds / 1000.0 /
                                 1000.0
                          << " Mbit/s"
                          << ", " << (data.id - packets_received)
                          << " packets lost"
                          << ", " << out_of_order_delivered_packets
                          << " packets delivered out-of-order" << std::endl;
                last_print = clk::now();
              }
            }
            this->receive();
          }
        });
  }
};

int main(int argc, char *argv[]) {
  Receiver r(std::stoi(argv[1]));
  r.run();
}
